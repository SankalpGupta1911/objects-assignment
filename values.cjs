function values(obj){
    if (obj == undefined || typeof(obj) != "object"){
        return [];
    }
    const list = [];
    for(let key in obj){
        list.push(obj[key])
    }
    return list;
}

module.exports = values;