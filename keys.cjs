function keys(obj){
    if (obj == undefined || typeof(obj) != "object"){
        return [];
    }
    const list = [];
    for (let key in obj){
        list.push(key.toString());
    }
    return list;
}

module.exports = keys;