function getPair(obj){
    const list = [];
    if (obj == undefined || typeof(obj) != 'object'){
        return [];
    }
    else{
        for(let key in obj){
            const kvpair = [];
            kvpair.push(key);
            kvpair.push(obj[key]);
            list.push(kvpair);
        }
    }
    return list;
}

module.exports = getPair;