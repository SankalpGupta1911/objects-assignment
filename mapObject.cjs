function mapObject(obj){
    if (typeof(obj) != "object"){
        return "incorrect input";
    }
    const list = [];
    function mapper(object,k){
        if(typeof(object[k])=='number'){
            return object[k]+100;
        }
        else if(typeof(object[k])=='string'){
            return "modified"+object[k];
        }
        return "X"
    }
    for(let key in obj){
        list.push(mapper(obj,key));
        }
    return list;
}

module.exports = mapObject;