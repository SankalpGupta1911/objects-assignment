function mapObject(obj){
    if (typeof(obj) != "object"){
        return "incorrect input";
    }
    const list = {};
    for(let key in obj){
        list[obj[key]] = key;
    }
    return list;
}

module.exports = mapObject;