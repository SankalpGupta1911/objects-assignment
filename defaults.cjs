function defaults(obj, defaultProps) {
    for(let defaultkey in defaultProps){
        let count = undefined;
        for(let key in obj){
            if(key == defaultkey){
                count = 1;
                break;
            }
        }
        if(count != 1){
            obj[defaultkey] = defaultProps[defaultkey];
        }
    }
    return obj;
}

module.exports = defaults;